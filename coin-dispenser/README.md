## Coin Dispenser using Micronaut 3.3.0

For more information about framework
- [User Guide](https://docs.micronaut.io/3.3.0/guide/index.html)

---

Create .env file in the root directory of project

####Below are variable that MUST be set.
 - SERVICE_IMAGE=dispenser:0.0.1 (Image name)
 - SERVICE_PORT=8080 (Service Port)
 - SERVICE_CONTAINER_PORT=8080 (Service Container Port)
 - SECURITY_ENABLED=false (Set if security is enabled or not)
 - MONGO_HOST=mongo  (Host of MongoDB)
 - MONGO_DB_COLLECTION=dc ( Mongo collection )
 - MONGO_PORT=27017 ( Mongo Port )
 - MONGO_DB_NAME=dispenser ( Mongo Database Name )
 - MONGO_DB_USER=coin ( Mongo Database User )
 - MONGO_DB_PASSWORD=123456 ( Mongo Database Password )
 - JWT_GENERATOR_SIGNATURE_SECRET= ( Security signature )

### Running the service ###

```bash
  $ docker network create my-net
  $ docker-compose up --build -d


**Note:** See API Docs http://service-host-here:service-port-here/api/v1/docs

# License

Copyright (c) 2022 Nicolas