package za.co.fnb;

import com.mongodb.client.model.Filters;
import io.micronaut.context.annotation.Value;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import reactor.core.publisher.Flux;
import za.co.fnb.repository.mongo.MongoConfig;

/**
 * @author Nicolas
 * @date 2022/02/10
 */
@Singleton
public class DBUtil {

    @Value("${db.collection-prefix}")
    private String collectionPrefix;

    @Inject
    private MongoConfig config;

    public void clearDb() {
        var database = config.getDatabase();
        Flux.from(database.listCollectionNames())
                .filter(it -> it.startsWith(collectionPrefix))
                .flatMap(it -> database.getCollection(it)
                        .deleteMany(Filters.empty()))
                .blockLast();
    }
}
