package za.co.fnb;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Nicolas
 * @date 2022/02/10
 */
public abstract class BaseCoinTester {

    protected void assertCoins(List<Integer> expectedList, List<Integer> actualList) {
        assertEquals(expectedList.size(), actualList.size());
        actualList.forEach(actual -> assertTrue(expectedList.contains(actual)));
    }
}
