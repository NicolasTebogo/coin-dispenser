package za.co.fnb;

import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import jakarta.inject.Inject;
import org.junit.jupiter.api.TestInstance;
import reactor.test.StepVerifier;
import za.co.fnb.business.CoinDispenserService;
import za.co.fnb.dto.ChangeRequestDTO;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
/**
 * @author Nicolas
 * @date 2022/02/10
 */
@MicronautTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CoinDispenserTest extends BaseCoinTester {

    @Inject protected CoinDispenserService service;

    @Inject DBUtil dbUtil;

    private String id;

    @BeforeAll
    void init() { dbUtil.clearDb();}

    @Test
    void testRequest1() {
        var request = ChangeRequestDTO
                .builder()
                .amount(6)
                .coins(List.of(9, 6, 5, 2))
                .build();
        var response = service.minCoinRequired(request);

        //this should pass as there is one coin 6 in
        // the coins that can make the amount
        StepVerifier.create(response)
                .consumeNextWith(it -> {
                    assertNotNull(it);
                    assertNotNull(it.getId());
                    assertEquals(request.getAmount(), it.getAmount());
                    assertEquals(1, (int) it.getMinimumChange());
                    assertCoins(request.getCoins(), it.getCoins());

                    id = it.getId();

                })
                .verifyComplete();

        //Test getBy ID
        //this should pass as there is one coin 6 in
        // the coins that can make the amount
        StepVerifier.create(service.get(id))
                .consumeNextWith(it -> {
                    assertNotNull(it);
                    assertNotNull(it.getId());
                    assertEquals(request.getAmount(), it.getAmount());
                    assertEquals(1, (int) it.getMinimumChange());
                    assertCoins(request.getCoins(), it.getCoins());

                })
                .verifyComplete();

    }

    @Test
    void testRequest2() {
        var request = ChangeRequestDTO
                .builder()
                .amount(1)
                .coins(List.of(9, 2, 3, 7))
                .build();
        var response = service.minCoinRequired(request);

        //this should fail or return zero since the amount is less than any of the coins
        StepVerifier.create(response)
                .consumeNextWith(it -> {
                    assertNotNull(it);
                    assertNotNull(it.getId());
                    assertEquals(request.getAmount(), it.getAmount());
                    assertEquals(0, (int) it.getMinimumChange());
                    assertCoins(request.getCoins(), it.getCoins());

                })
                .verifyComplete();

    }

    @Test
    void testRequest3() {
        var request = ChangeRequestDTO
                .builder()
                .amount(10)
                .coins(List.of(5, 2, 3, 7))
                .build();
        var response = service.minCoinRequired(request);

        //this should return two as there are two coins that can make amount 10 (7, 3)
        StepVerifier.create(response)
                .consumeNextWith(it -> {
                    assertNotNull(it);
                    assertNotNull(it.getId());
                    assertEquals(request.getAmount(), it.getAmount());
                    assertEquals(2, (int) it.getMinimumChange());
                    assertCoins(request.getCoins(), it.getCoins());

                })
                .verifyComplete();

    }

    @Test
    void testRequest4() {
        var request = ChangeRequestDTO
                .builder()
                .amount(15)
                .coins(List.of(5, 2, 3, 2))
                .build();
        var response = service.minCoinRequired(request);

        //this should return two as there are three coins that can make amount 15 (5, 5, 5)
        StepVerifier.create(response)
                .consumeNextWith(it -> {
                    assertNotNull(it);
                    assertNotNull(it.getId());
                    assertEquals(request.getAmount(), it.getAmount());
                    assertEquals(3, (int) it.getMinimumChange());
                    assertCoins(request.getCoins(), it.getCoins());

                })
                .verifyComplete();

    }

}
