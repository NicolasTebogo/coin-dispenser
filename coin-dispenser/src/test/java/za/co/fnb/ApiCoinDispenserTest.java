package za.co.fnb;

import io.micronaut.core.type.Argument;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.BlockingHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import io.micronaut.reactor.http.client.ReactorHttpClient;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import za.co.fnb.dto.ApiExceptionDTO;
import za.co.fnb.dto.ChangeDTO;
import za.co.fnb.dto.ChangeRequestDTO;

import java.util.List;
import java.util.Optional;

import static io.micronaut.core.type.Argument.of;
import static io.micronaut.http.HttpRequest.GET;
import static io.micronaut.http.HttpRequest.POST;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Nicolas
 * @date 2022/02/10
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@MicronautTest
public class ApiCoinDispenserTest extends BaseCoinTester {

    @Inject
    @Client("/api/v1/dispenser")
    ReactorHttpClient client;

    @Test
    void createRequest1() {
        //Minimum coins to make 7 from {1, 2, 3} is 3 this test should pass
        var dto = ChangeRequestDTO
                .builder()
                .amount(7)
                .coins(List.of(1, 2, 3))
                .build();

        var response = blockingClient().retrieve(
                POST("/", dto),
                of(ChangeDTO.class));

        assertNotNull(response);
        assertNotNull(response.getId());
        assertEquals(dto.getAmount(), response.getAmount());
        assertEquals(3, (int) response.getMinimumChange());
        assertCoins(dto.getCoins(), response.getCoins());
    }

    @Test
    void createRequest2() {
        //Minimum coins to make 15 from {1, 2, 3} is 5 this test should pass
        var dto = ChangeRequestDTO
                .builder()
                .amount(15)
                .coins(List.of(1, 2, 3))
                .build();

        var response = blockingClient().retrieve(
                POST("/", dto),
                of(ChangeDTO.class));

        assertNotNull(response);
        assertNotNull(response.getId());
        assertEquals(dto.getAmount(), response.getAmount());
        assertEquals(5, (int) response.getMinimumChange());
        assertCoins(dto.getCoins(), response.getCoins());

        //Test get by ID
        var response1 = blockingClient().retrieve(
                GET("/"+response.getId()), of(ChangeDTO.class));
        assertNotNull(response1);
        assertEquals(response.getId(), response1.getId());
        assertEquals(response.getAmount(), response1.getAmount());

    }

    @Test
    void createValidation() {
        var dto = ChangeRequestDTO
                .builder()
                .coins(List.of(1, 2, 3))
                .build();

        HttpClientResponseException ex = assertThrows(HttpClientResponseException.class,
                () -> blockingClient().retrieve(POST("/", dto),
                        Argument.of(ApiExceptionDTO.class)));

        HttpResponse<?> response = ex.getResponse();
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatus());
        Optional<ApiExceptionDTO> responseOpt = response.getBody(ApiExceptionDTO.class);

        assertTrue(responseOpt.isPresent());
        ApiExceptionDTO apiErrorDTO = responseOpt.get();
        assertEquals(HttpStatus.BAD_REQUEST, apiErrorDTO.getStatus());

        List<String> errors = apiErrorDTO.getErrors();
        assertEquals(1, errors.size());
        assertTrue(errors.contains("amount must not be null"));
    }

    private BlockingHttpClient blockingClient() {return client.toBlocking();}
}
