package za.co.fnb.business.base;

import jakarta.inject.Inject;
import za.co.fnb.mapper.ObjectMapper;
import za.co.fnb.repository.Repository;

import java.util.Objects;

/**
 * @author Nicolas
 * @date 2022/02/08
 */
public abstract class BaseService {

    @Inject
    protected ObjectMapper mapper;

    @Inject protected Repository repository;

    protected boolean notNull(Object value) {
        return !isNull(value);
    }

    protected boolean isNull(Object value) {
        return Objects.isNull(value);
    }
}
