package za.co.fnb.business;

import jakarta.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import za.co.fnb.business.base.BaseService;
import za.co.fnb.dto.ChangeDTO;
import za.co.fnb.dto.ChangeRequestDTO;
import za.co.fnb.model.Change;
import za.co.fnb.utils.MathUtil;

/**
 * @author Nicolas
 * @date 2022/02/08
 */
@Slf4j
@Singleton
public class CoinDispenserService extends BaseService {

    /**
     * This method finds the minimum number of coins needed for a given amount.
     *
     * @param requestDTO DTO containing a list of coins and target amount
     *                   Finds the the minimum number of coins that make a given value.
     **/
    public Mono<ChangeDTO> minCoinRequired(ChangeRequestDTO requestDTO) {
        var change = mapper.map(Change.class, requestDTO);
        var count = MathUtil.minCoins(requestDTO.getCoins(),
                requestDTO.getAmount());
        change.setMinimumChange(count);
        return repository.save(change)
                .map(e -> mapper.map(ChangeDTO.class, e));
    }

    public Mono<ChangeDTO> get(String id) {
        return repository.get(Change.class, id)
                .map(change -> mapper.map(ChangeDTO.class, change));
    }

    public Flux<ChangeDTO> get() {
        return repository.get(Change.class)
                .map(change -> mapper.map(ChangeDTO.class, change));
    }
}
