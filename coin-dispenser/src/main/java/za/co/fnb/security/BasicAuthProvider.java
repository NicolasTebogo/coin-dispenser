package za.co.fnb.security;

import io.micronaut.http.HttpRequest;
import io.micronaut.security.authentication.AuthenticationException;
import io.micronaut.security.authentication.AuthenticationProvider;
import io.micronaut.security.authentication.AuthenticationRequest;
import io.micronaut.security.authentication.AuthenticationResponse;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Mono;

import java.security.spec.InvalidKeySpecException;
import java.util.Map;

import static io.micronaut.security.authentication.AuthenticationFailureReason.*;

/**
 * @author Nicolas
 * @date 2022/02/08
 */
public class BasicAuthProvider implements AuthenticationProvider {

    @Override
    public Publisher<AuthenticationResponse> authenticate(HttpRequest<?> httpRequest, AuthenticationRequest<?, ?> authenticationRequest) {
        final String username = authenticationRequest.getIdentity().toString();
        final String password = authenticationRequest.getSecret().toString();

        return Mono.just(AuthenticationResponse.failure(CREDENTIALS_DO_NOT_MATCH));
        /*var filter = EntityFilter
                .builder()
                .values(Map.of("email", username))
                .type(FilterType.AND)
                .build();

        final var pageable = Pageable.builder().filter(filter).build();

        return repository.get(User.class, pageable)
                .switchIfEmpty(Mono.error(() -> new AuthenticationException(
                        AuthenticationResponse.failure(USER_NOT_FOUND))))
                .map(user -> {
                    boolean isAuth = false;
                    try {
                        if (!user.getStatus().equals(User.Status.ACTIVE))
                            return AuthenticationResponse.failure(USER_DISABLED);

                        isAuth = provider.authenticate(password.toCharArray(),
                                user.getPassword(), user.getSalt());
                    } catch (InvalidKeySpecException ignored) {
                    }

                    if (isAuth) {
                        log.info("============================Logged In : "+username);
                        return AuthenticationResponse.success((String) authenticationRequest.getIdentity(),
                                user.getRoles(), Map.of("oid", user.hexId()));
                    }
                    return AuthenticationResponse.failure(CREDENTIALS_DO_NOT_MATCH);

                });*/
    }
}
