package za.co.fnb.model.base;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.types.ObjectId;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
/**
 * @author Nicolas
 * @date 2022/02/10
 */
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public abstract class Entity implements Serializable {

    /**
     * Unique identifier, optimized for database queries and referencing.
     */
    @BsonId
    @NotNull
    private ObjectId id;


    /**
     * Short description defining the instance.
     */
    @NotBlank
    private String description;

    /**
     * Timestamp when the record was created in the database.
     */
    @NotNull
    private LocalDateTime createdAt;

    /**
     * Timestamp when the record was last updated in the database.
     */
    @NotNull
    private LocalDateTime updatedAt;

    public String hexId() {
        return id != null ? id.toHexString() : null;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }
}
