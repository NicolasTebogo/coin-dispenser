package za.co.fnb.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import za.co.fnb.model.base.CollectionName;
import za.co.fnb.model.base.Entity;

import java.util.List;

/**
 * @author Nicolas
 * @date 2022/02/10
 */
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@ToString
@CollectionName("change")
public class Change extends Entity {

    private Integer amount;

    private List<Integer> coins;

    private Integer minimumChange;
}
