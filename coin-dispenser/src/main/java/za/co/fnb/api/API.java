package za.co.fnb.api;

import jakarta.inject.Inject;
import za.co.fnb.business.CoinDispenserService;
import za.co.fnb.mapper.ObjectMapper;

/**
 * @author Nicolas
 * @date 2022/02/10
 */
public abstract class API {

    @Inject
    protected ObjectMapper mapper;

    @Inject protected CoinDispenserService service;
}
