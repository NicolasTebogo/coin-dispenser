package za.co.fnb.api.exception;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Error;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.server.exceptions.ExceptionHandler;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import za.co.fnb.dto.ApiExceptionDTO;
import za.co.fnb.error.ErrorCode;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Stream;
/**
 * @author Nicolas
 * @date 2022/02/10
 */
@Produces(MediaType.APPLICATION_JSON)
@Singleton
public class GlobalExceptionHandler implements ExceptionHandler<Throwable, HttpResponse<?>> {

    @Error(global = true)
    @Override
    public HttpResponse<?> handle(HttpRequest request, Throwable t) {
        var errorCode = ErrorCode.OPERATION_FAIL;
        var response = ApiExceptionDTO.builder()
                .errorCode(errorCode)
                .errors(List.of(coalesce(t.getMessage(), "Unexpected internal error")))
                .traceId(UUID.randomUUID().toString())
                .status(errorCode.httpStatus())
                .timestamp(LocalDateTime.now())
                .build();

        return HttpResponse.<ApiExceptionDTO>serverError().body(response);
    }

    @SafeVarargs
    private <T> T coalesce(T... items) {
        return Stream.of(items).filter(Objects::nonNull).findFirst().orElse(null);
    }
}
