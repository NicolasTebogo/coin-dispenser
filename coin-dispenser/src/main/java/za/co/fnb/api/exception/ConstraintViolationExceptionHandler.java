package za.co.fnb.api.exception;

import io.micronaut.context.annotation.Replaces;
import io.micronaut.context.annotation.Requires;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.server.exceptions.ExceptionHandler;
import io.micronaut.validation.exceptions.ConstraintExceptionHandler;
import jakarta.inject.Singleton;
import za.co.fnb.dto.ApiExceptionDTO;
import za.co.fnb.error.ErrorCode;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;
/**
 * @author Nicolas
 * @date 2022/02/10
 */
@Produces
@Singleton
@Replaces(ConstraintExceptionHandler.class)
@Requires(classes = {ConstraintViolationException.class, ExceptionHandler.class})
public class ConstraintViolationExceptionHandler implements
        ExceptionHandler<ConstraintViolationException, HttpResponse<?>> {


    @Override
    public HttpResponse<?> handle(HttpRequest request, ConstraintViolationException ex) {
        Set<ConstraintViolation<?>> violations = ex.getConstraintViolations();
        return HttpResponse.badRequest(ApiExceptionDTO
                .builder()
                .errorCode(ErrorCode.VALIDATION_FAIL)
                .status(HttpStatus.BAD_REQUEST)
                .traceId(UUID.randomUUID().toString())
                .errors(violationsMessages(violations))
                .timestamp(LocalDateTime.now())
                .build());
    }

    public List<String> violationsMessages(Set<ConstraintViolation<?>> violations) {
        return violations.stream()
                .map(this::violationMessage)
                .toList();
    }

    private String violationMessage(ConstraintViolation<?> violation) {
        var sb = new StringBuilder();
        var lastNode = lastNode(violation.getPropertyPath());
        if (lastNode != null) {
            sb.append(lastNode.getName());
            sb.append(" ");
        }
        sb.append(violation.getMessage());
        return sb.toString();
    }

    private Path.Node lastNode(Path path) {
        Path.Node lastNode = null;
        for (final Path.Node node : path)
            lastNode = node;
        return lastNode;
    }
}

