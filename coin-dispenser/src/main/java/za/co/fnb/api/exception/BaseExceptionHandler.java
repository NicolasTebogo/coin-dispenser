package za.co.fnb.api.exception;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.server.exceptions.ExceptionHandler;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import za.co.fnb.dto.ApiExceptionDTO;
import za.co.fnb.error.BaseException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Stream;
/**
 * @author Nicolas
 * @date 2022/02/10
 */
@Produces(MediaType.APPLICATION_JSON)
@Singleton
public class BaseExceptionHandler<E extends BaseException> implements
        ExceptionHandler<E, HttpResponse<?>> {

    @Override
    public HttpResponse<?> handle(HttpRequest request, E exception) {
        var errorCode = exception.getErrorCode();
        var response = ApiExceptionDTO.builder()
                .errorCode(errorCode)
                .status(errorCode.httpStatus())
                .traceId(UUID.randomUUID().toString())
                .errors(List.of(coalesce(exception.getMessage(), "Internal Error")))
                .timestamp(LocalDateTime.now())
                .build();

        return HttpResponse
                .status(errorCode.httpStatus())
                .body(response);
    }

    @SafeVarargs
    private <T> T coalesce(T... items) {
        return Stream.of(items).filter(Objects::nonNull).findFirst().orElse(null);
    }
}

