package za.co.fnb.api;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import za.co.fnb.dto.ChangeDTO;
import za.co.fnb.dto.ChangeRequestDTO;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;

/**
 * @author Nicolas
 * @date 2022/02/08
 */
@Controller("/dispenser")
@Tag(name = "Coin Dispenser")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Secured(SecurityRule.IS_AUTHENTICATED)
@Validated
@Introspected
public class DispenserApi extends API {

    @PermitAll
    @Operation(description = "Retrieves all stored change request")
    @Get
    Flux<ChangeDTO> getChanges() {
        return service.get();
    }

    @PermitAll
    @Operation(description = "Retrieves change matching ID")
    @Get("/{id}")
    Mono<ChangeDTO> getChanges(String id) {
        return service.get(id);
    }

    @PermitAll
    @Operation(description = "Request minimum change")
    @Post
    Mono<ChangeDTO> getChange(@Valid @Body ChangeRequestDTO requestDTO) {
        return service.minCoinRequired(requestDTO);
    }

}
