package za.co.fnb.repository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import za.co.fnb.model.base.Entity;

import javax.validation.Valid;
import java.util.Map;
/**
 * @author Nicolas
 * @date 2022/02/10
 */
public interface Repository {

    <T extends Entity> Mono<T> save(@Valid  T candidate);

    <T extends Entity> Flux<T> get(Class<T> type, Map<String, Object> filters);

    <T extends Entity> Mono<T> get(Class<T> type, String id);

    <T extends Entity> Mono<Long> count(Class<T> type);

    <T extends Entity> Flux<T> get(Class<T> type);

    <T extends Entity> Mono<Boolean> delete(Class<T> type, String id);
}
