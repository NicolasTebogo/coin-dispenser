package za.co.fnb.repository.mongo;

import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import com.mongodb.reactivestreams.client.MongoCollection;
import io.micronaut.context.annotation.Value;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.bson.conversions.Bson;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import za.co.fnb.model.base.Entity;
import za.co.fnb.repository.Repository;

import java.util.Map;

import static com.mongodb.client.model.Filters.eq;
import static za.co.fnb.utils.ValidationUtil.toId;
/**
 * @author Nicolas
 * @date 2022/02/10
 */
@Singleton
@Slf4j
public class MongoRepository implements Repository {

    @Value("${micronaut.data.paging.max-page-size}")
    private int maxPageSize;

    @Value("${micronaut.data.paging.default-page}")
    private int defaultPage;

    @Inject
    private MongoConfig config;

    public <T extends Entity> MongoCollection<T> collection(Class<T> clazz) {
        return config.collection(clazz);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends Entity> Mono<T> save(T entity) {
        return Mono.from(collection((Class<T>) entity.getClass())
                        .insertOne(entity))
                        .map(it -> entity);
    }

    @Override
    public <T extends Entity> Flux<T> get(Class<T> type, Map<String, Object> filter) {
        // filtering
        final var clause = filters(filter);

        return Flux.from(collection(type)
                .find(clause)
                .skip(defaultPage)
                .limit(maxPageSize));
    }

    @Override
    public <T extends Entity> Mono<T> get(Class<T> type, String id) {
        return Mono.from(collection(type)
                .find(eq("_id", toId(id)))
                .limit(1));
    }

    @Override
    public <T extends Entity> Mono<Long> count(Class<T> type) {
        return Mono.from(collection(type).countDocuments());
    }

    @Override
    public <T extends Entity> Mono<Boolean> delete(Class<T> type, String hexId) {
        final var result = collection(type).deleteOne(eq("_id", toId(hexId)));
        return Mono.from(result).map(it -> it.getDeletedCount() == 1);
    }

    private Bson filters(Map<String, Object> fieldValues) {
        return Filters.and(fieldValues.entrySet()
                .stream()
                .map(it -> Updates.set(it.getKey(), it.getValue()))
                .toList());
    }

    @Override
    public <T extends Entity> Flux<T> get(Class<T> type) {
        return Flux.from(collection(type).find());
    }
}
