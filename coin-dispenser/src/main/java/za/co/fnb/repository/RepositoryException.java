package za.co.fnb.repository;
/**
 * @author Nicolas
 * @date 2022/02/10
 */
public class RepositoryException extends RuntimeException {
    public RepositoryException(String message) {
        super(message);
    }
}
