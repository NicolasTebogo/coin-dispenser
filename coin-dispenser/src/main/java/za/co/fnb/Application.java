package za.co.fnb;

import io.micronaut.runtime.Micronaut;
import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.*;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;

@OpenAPIDefinition(
        info = @Info(
                title = "${api.title}",
                version = "${api.version}",
                description = "${api.description}",
                license = @License(
                        name = "${api.licence}",
                        url = "${api.licence.url}"),
                contact = @Contact(
                        url = "${api.contact.url}",
                        name = "${api.contact.name}",
                        email = "${api.contact.email}")
        )
)
@SecurityScheme(name = "BearerAuth",
        type = SecuritySchemeType.HTTP,
        scheme = "bearer",
        bearerFormat = "jwt")

@SecurityRequirement(name = "BearerAuth")
public class Application {

    public static void main(String[] args) {
        Micronaut.run(Application.class, args);
    }
}
