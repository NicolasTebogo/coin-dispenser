package za.co.fnb.dto.base;

import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
/**
 * @author Nicolas
 * @date 2022/02/10
 */
@Introspected
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public abstract class FilterDTO extends ServerDTO {

    public int page;

    public int size;

    public String orderBy;

    public OrderDirection order;

    public enum OrderDirection {ASC, DESC}
}


