package za.co.fnb.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import za.co.fnb.dto.base.EntityDTO;

import java.util.List;

/**
 * @author Nicolas
 * @date 2022/02/10
 */
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ChangeDTO extends EntityDTO  {

    private Integer amount;

    private List<Integer> coins;

    private Integer minimumChange;
}
