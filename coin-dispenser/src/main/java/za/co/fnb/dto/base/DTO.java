package za.co.fnb.dto.base;

import io.micronaut.core.annotation.Introspected;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * Base class for all DTOs
 */
@Introspected
@SuperBuilder
@NoArgsConstructor
public abstract class DTO implements Serializable {}
