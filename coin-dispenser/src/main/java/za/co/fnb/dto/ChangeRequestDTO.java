package za.co.fnb.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import za.co.fnb.dto.base.ServerDTO;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author Nicolas
 * @date 2022/02/08
 */
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ChangeRequestDTO extends ServerDTO {

    @NotEmpty
    @Size(min = 1)
    private List<Integer> coins;

    @NotNull
    @Positive
    private Integer amount;
}
