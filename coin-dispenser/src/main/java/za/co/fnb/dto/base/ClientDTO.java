package za.co.fnb.dto.base;

import io.micronaut.core.annotation.Introspected;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * Base class for client bound DTOs
 */
@Introspected
@SuperBuilder
@NoArgsConstructor
public abstract class ClientDTO extends DTO {}
