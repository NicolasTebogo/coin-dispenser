package za.co.fnb.dto;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.http.HttpStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import za.co.fnb.dto.base.ClientDTO;
import za.co.fnb.error.ErrorCode;

import java.time.LocalDateTime;
import java.util.List;
/**
 * @author Nicolas
 * @date 2022/02/10
 */
@Introspected
@Setter
@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class ApiExceptionDTO extends ClientDTO {

    private String traceId;

    private ErrorCode errorCode;

    private HttpStatus status;

    private List<String> errors;

    private LocalDateTime timestamp;
}
