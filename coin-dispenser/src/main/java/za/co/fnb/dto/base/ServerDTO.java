package za.co.fnb.dto.base;

import io.micronaut.core.annotation.Introspected;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * Base class for server bound DTOs
 */
@Introspected
@SuperBuilder
@NoArgsConstructor
public abstract class ServerDTO extends DTO {}
