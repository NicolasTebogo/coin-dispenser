package za.co.fnb.mapper.converters;

import jakarta.inject.Singleton;
import org.bson.types.ObjectId;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import za.co.fnb.dto.ChangeRequestDTO;
import za.co.fnb.model.Change;

import java.time.LocalDateTime;

/**
 * @author Nicolas
 * @date 2022/02/10
 */
@Singleton
public class Mappings {

    public void add(ModelMapper mapper) {
        changeDTO(mapper);
    }

    public void changeDTO(ModelMapper mapper) {
        mapper.addMappings(new PropertyMap<ChangeRequestDTO, Change>() {
            @Override
            protected void configure() {
                map(LocalDateTime.now(), destination.getCreatedAt());
                map(LocalDateTime.now(), destination.getUpdatedAt());
            }
        });
    }

    private final Converter<ObjectId, String> hexId = (ctx)
            -> ctx.getSource().toHexString();
}
