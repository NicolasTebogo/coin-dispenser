package za.co.fnb.utils;

import org.bson.types.ObjectId;
import za.co.fnb.error.OperationFailedException;

import java.util.Objects;

/**
 * @author Nicolas
 * @date 2022/02/08
 */
public class ValidationUtil {
    public static boolean isNull(Object value) {
        return Objects.isNull(value);
    }

    public static ObjectId toId(String hex) {
        if (ObjectId.isValid(hex)) return new ObjectId(hex);
        throw new OperationFailedException(String.format("Invalid id '%s'", hex));
    }
}
