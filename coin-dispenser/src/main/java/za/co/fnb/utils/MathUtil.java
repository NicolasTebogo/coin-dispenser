package za.co.fnb.utils;

import java.util.List;

/**
 * @author Nicolas
 * @date 2022/02/09
 */
public final class MathUtil {

    // m is size of coins array (number of different coins)
    public static int minCoins(List<Integer> coins, int amount) {
        // base case
        if (amount == 0) return 0;

        // Initialize result
        int res = Integer.MAX_VALUE;

        // Try every coin that has smaller value than V
        for (int i = 0; i < coins.size(); i++) {
            //if value of coin is greater than total we are looking for just continue.
            if( coins.get(i) > amount ) continue;

            if (coins.get(i) <= amount) {

                int subResult = minCoins(coins, amount - coins.get(i));

                // Check for INT_MAX to avoid overflow and see if
                // result can minimized
                if (subResult != Integer.MAX_VALUE && subResult + 1 < res)
                    res = subResult + 1;
            }
        }
        return res == Integer.MAX_VALUE ? 0 : res;
    }
}
