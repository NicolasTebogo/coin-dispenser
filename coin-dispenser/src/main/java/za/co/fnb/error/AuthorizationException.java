package za.co.fnb.error;
/**
 * @author Nicolas
 * @date 2022/02/10
 */
public class AuthorizationException extends RuntimeException {

    public AuthorizationException(String message) {super(message);}

}
