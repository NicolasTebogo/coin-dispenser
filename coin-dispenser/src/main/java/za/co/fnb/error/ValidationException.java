package za.co.fnb.error;
/**
 * @author Nicolas
 * @date 2022/02/10
 */
public class ValidationException extends BaseException {

    public ValidationException(String message) {
        super(message);
    }

    @Override
    public ErrorCode getErrorCode() {
        return ErrorCode.VALIDATION_FAIL;
    }
}
