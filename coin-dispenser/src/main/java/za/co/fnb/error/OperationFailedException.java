package za.co.fnb.error;
/**
 * @author Nicolas
 * @date 2022/02/10
 */
public class OperationFailedException extends BaseException {

    public OperationFailedException(String message, Object... args) {
        super(String.format(message, args));
    }

    public OperationFailedException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public ErrorCode getErrorCode() {
        return ErrorCode.OPERATION_FAIL;
    }
}
