package za.co.fnb.error;

import io.micronaut.http.HttpStatus;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
/**
 * @author Nicolas
 * @date 2022/02/10
 */
public enum ErrorCode {

    UNEXPECTED(5000, HttpStatus.INTERNAL_SERVER_ERROR),
    VALIDATION_FAIL(4002, HttpStatus.BAD_REQUEST),
    OPERATION_FAIL(4010, HttpStatus.BAD_REQUEST),
    RECORD_NOT_FOUND(4011, HttpStatus.NOT_FOUND);

    static {
        // codes uniqueness check
        var codes = new HashSet<Integer>();
        for (ErrorCode value : ErrorCode.values()) {
            if (!codes.add(value.code())) {
                LoggerFactory.getLogger(ErrorCode.class)
                        .warn("Multiple {} values refer to code {}", ErrorCode.class, value.code);
            }
        }
    }

    private final int code;

    private final HttpStatus httpStatus;

    ErrorCode(int code, HttpStatus httpStatus) {
        this.code = code;
        this.httpStatus = httpStatus;
    }

    public int code() { return code; }

    public HttpStatus httpStatus() { return httpStatus; }
}
