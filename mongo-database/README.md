# mongodb
Mongo Database Single Instance

### Running the service ###

Create .env file in the root directory of project 

####Below are variable that MUST be set.
  - MONGODB_PORT=27017 (Database port)
  - MONGODB_DATABASE=dispenser (Database name)
  - MONGODB_USERNAME=coin (Database user)
  - MONGODB_PASSWORD=123456 (Database password)
  - MONGODB_ROOT_PASSWORD=123456  (Database root password)


create Network and volume

```bash
  $ docker volume create --name=mongo-data
  $ docker network create my-net
  $ docker-compose up --build -d

# License

Copyright (c) 2022 Nicolas