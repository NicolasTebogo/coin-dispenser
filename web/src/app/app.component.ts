import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'coin-dispenser-web';

  nameModel: string = "";
  amount: number = 0;
  listOfCoins = [];
  answer : number = 0;

  constructor(private httpClient: HttpClient) { }

  ngOnInit() { }




onSubmit() {
  this.listOfCoins.push(this.nameModel)
  console.log(this.listOfCoins);
  this.nameModel = '';
}


  public makeRequest() {


    var body = {
      "amount" : this.amount,
      "coins" : this.listOfCoins
    };
    console.log(body);

    this.getHttpResults( body ).subscribe(
      res => {
        console.log(res);
        this.answer = res.minimumChange;
      }, error => {
        console.log(error);
      }
    )
  }

  public getHttpResults( body : any ): Observable<any> {
    return this.httpClient.post("http://localhost:8080/api/v1/dispenser/", body,
      {
        headers: new HttpHeaders().set("Accept", 'application/json')
        .set('Content-Type', 'application/json')
      });
  }
}
